package com.devcamp.customeraccount.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccount.models.Account;

@Service
public class AccountService extends CustomerService{
    Account account1 = new Account(001, customer1, 20000.0);
    Account account2 = new Account(002, customer2, 340000000.0);
    Account account3 = new Account(003, customer3, 56000000.0);
    public ArrayList<Account> getAllAccounts(){
        ArrayList<Account> accountList = new ArrayList<>();
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;

    
}
}
