package com.devcamp.customeraccount.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccount.models.Account;
import com.devcamp.customeraccount.services.AccountService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AccountController {
    @Autowired
    AccountService accountService;
    @GetMapping("/accounts")
    public ArrayList<Account> getAllAccount(){
        return accountService.getAllAccounts();
    }

    
}
