package com.devcamp.customeraccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomeraccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomeraccountApplication.class, args);
	}

}
